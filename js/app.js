define([
  'jquery',
  'underscore',
  'backbone',
  'jqgrid',
  'gridLocale',
  'router' // Request router.js
  
], function($, _, Backbone, jqGrid, gridLocale, Router){
  var initialize = function(){

    Router.initialize();
  }

  return {
    initialize: initialize
  };
});
