define([
  'jquery',
  'underscore',
  'backbone',
  'jqgrid',
  'gridLocale',
  'collections/objects',
  'text!templates/jqgrid.html',
  'models/object'
  
], function($, _, Backbone, jqgrid, gridLocale, jqGridCollection, jqgridTemplate, Object){
	
	var counter_id = 0;

	var jqGridView = Backbone.View.extend({
		
		template: jqgridTemplate,

		events: {
			'click #controls a#link-add-row' : 'addRowOnjqGrid',
			'click .gridbutton' : 'deleteRowOnjqGrid',
		},
		
		initialize: function() {
			this.collection = new jqGridCollection();
		},
		
		populateJqGrid: function(event){
			event.preventDefault();
			this.collection.fetch();
		},

		addRowOnjqGrid: function(event){
			var parameters = {
				rowID : counter_id++,
		    initdata : {
		    	"event-name" : "Player A vs Player B",
		    	"sport" : "Soccer",
		    	"side" : "win"
		    },
		    position :"last",
		    useDefValues : false,
		    useFormatter : false,
		    addRowParams : {extraparam:{}}
			}
			this.$el.find("#grid-test").jqGrid('addRow',parameters);
		},

		deleteRowOnjqGrid: function(e){
			e.preventDefault();
			$('#grid-test').jqGrid('delRowData',$(e.target).attr("data-id"));
		},
		
		render: function() {
			this.$el.html(_.template(this.template));

			this.$el.find("#grid-test").jqGrid({
				datatype: 'local',
	      colModel: [
	        {name: 'event-name', label: 'Event', width: 250},
	        {name: 'sport', label: "Sport", width: 250},
	        {name: 'side', label: 'Side', width: 250},
	        {name: 'edit', index: 'edit', align: 'center', sortable: false}
	      ],
	      gridview: true,
	      rownumbers: true,
	      subGrid: true,
	      afterInsertRow: function(id, currentData, jsondata) {
		        var delete_button = "<a class='gridbutton' data-id='"+id+"' href='#'>Delete</a>";
		        $(this).setCell(id, "edit", delete_button);
		    },
	      height: "100%",
	      caption: 'Test Grid',
	      pager: $("#pager-grid-test"),
	      subGridRowExpanded: function (subgridId, rowid) {
          var subgridTableId = subgridId + "_t";
          $("#" + subgridId).html("<table id='" + subgridTableId + "'></table>");
          $("#" + subgridTableId).jqGrid({
              datatype: "local",
              data: [{
              	"test" : "test",
              	"testtype" : "test2"
              },
              {
              	"test" : "tes3",
              	"testtype" : "test4"
              }],
              colNames: ["Test", "TestType"],
              colModel: [
                {name: "test", width: 130},
                {name: "testtype", width: 130}
              ],
              height: "100%",
              sortname: "test",
              idPrefix: "s_" + rowid + "_"
          });
        }
	    }).jqGrid('navGrid','#pager-grid-test', {edit:false, add:false, del:false, search:false, refresh:false});


			return this;
		}
	});
	
  return jqGridView;
  
});