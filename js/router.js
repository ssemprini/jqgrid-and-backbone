define([
  'jquery',
  'underscore',
  'backbone',
  'views/todos/action',
  'collections/objects'

], function($, _, Backbone, jqGridView, jqGridCollection){
	
	var AppRouter = Backbone.Router.extend({
        routes: {
        	
        	"" : "home", 
        	
            "*actions": "defaultAction"
        },
        
        initialize : function(){
        	
        }
	
	
    });

  var initialize = function(){
	  console.log("Initialize Router");
	  
	    // Initiate the router
	  var app_router = new AppRouter;
	  
	  	app_router.on('route:home', function() {

	        var jqgridView = new jqGridView({
            el: $('#container-actions'),
          });
	        jqgridView.render();
	    });
	  	
	    app_router.on('route:defaultAction', function(actions) {
	        console.log("Route: "+actions);
	    });

	    Backbone.history.start();
  };
  
  	return {
  		initialize: initialize
  	};
  
});
