define([
  'underscore',
  'backbone',
  // Pull in the Model module from above
  'models/object'
], function(_, Backbone, Object){
	
	var ObjectList = Backbone.Collection.extend({
		
		model: Object,

		url: 'http://localhost:3000/jqgrid.json',
		
		parse: function(response) {
			console.log("PARSE", response);
		    return response;
		  }
		
	});
  // You don't usually return a collection instantiated
  return ObjectList;
});